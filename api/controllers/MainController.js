/**
 * MainController
 *
 * @description :: Server-side logic for managing mains
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	/*index: function (req,res){
		//index action
	},

	payslip: function(req, res){
		//payslip action

	},*/
	
	calculate: function(req, res){
		//-----------------START get params-----------------//
		var name = req.param('name');
		var lastName = req.param('lastName');
		var annualsalary = req.param('annualsalary');
		var pensionrate = req.param('pensionrate');
		var paymentDate = req.param('paymentDate');
		var paymentDateEnd = req.param('paymentDateEnd');
		//-----------------END get params-----------------//

		//--------START call services(api/services/calc.js)----------//
		var grossincome = calc.grossIncome(annualsalary);
		var incometax = calc.incomeTax(annualsalary);
		var netincome = calc.netIncome(grossincome,incometax);
		var pensioncontrib = calc.pensionContribuition(grossincome,pensionrate);
		//-------------------END call services)----------------------//
	
		var params = {
			name: name, 
			lastName: lastName, 
			paymentDate: paymentDate, 
			grossincome: grossincome,
			incometax: incometax, 
			netincome: netincome, 
			pensioncontribuition: pensioncontrib, 
			paymentDateEnd: paymentDateEnd
		};

		res.json(params);

	}
	
};

