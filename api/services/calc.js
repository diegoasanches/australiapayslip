module.exports = {

	grossIncome: function(annualsalary){
		//gross income
		var grossIncome = parseFloat((annualsalary/12).toFixed(3));
		grossIncome = grossIncome*1000;

		return grossIncome;
	},

	incomeTax: function(annualsalary){
		//income tax
		var result = 0;

		if(annualsalary>=18.201 && annualsalary<=37.000){
			result = ((annualsalary-18.000)*0.19)/12;
		}
		else if(annualsalary>=37.001 && annualsalary<=80.000){
			result = (((annualsalary-37.000)*0.325)+3.572)/12;
		}
		else if(annualsalary>=80.001 && annualsalary<=180.000){
			result = (((annualsalary-80.000)*0.37)+17.547)/12;
		}
		else if(annualsalary>=180.001){
			result = (((annualsalary-180.000)*0.45)+54.547)/12;
		}

		result = parseFloat(result).toFixed(3);

		return (result*1000);

	},

	netIncome: function(grossIncome, incomeTax){
		//net income
		var netIncome = grossIncome - incomeTax;

		return netIncome;
	},

	pensionContribuition: function(grossIncome, pensionRate){
		//pension contribuition
		var pensionrate = pensionRate/100;
		var pensionContribuition = parseFloat(grossIncome * pensionrate).toFixed(0);

		return pensionContribuition;
	}

};