//----------------Mask--------------
$("#annualsalaryInput").maskMoney({showSymbol:true, symbol:"$", decimal:".", thousands:",", precision: 3});
$('#paymentDate').mask("99/99");
$('#paymentDateEnd').mask("99/99");
//----------------Mask END--------------

//clean the "pension rate" form when click on it
$('#pensionrate').click(function(){
	$('#pensionrate').val('');
});

//listener for calculate button
$('#btnCalculate').click(function(){

	//---------------get the forms value---------------//
	var $nameInput = $('#nameInput').val();
	var $lastnameInput = $('#lastnameInput').val();
	var $annualsalaryInput = $('#annualsalaryInput').val();
	var $pensionrateInput = $('#pensionrate').val();
	var $paymentDate = $('#paymentDate').val();
	var $paymentDateEnd = $('#paymentDateEnd').val();
	//---------------get the forms value END---------------//


	//call a function to change the currency format to number format
	var $annualsalaryInput = currencyToNumber($annualsalaryInput);
	var $pensionrateInput = currencyToNumber($pensionrateInput);


	//----------------------form validate----------------------//

	//if the form is ok
	if(validateForm($nameInput, $lastnameInput, $annualsalaryInput, $pensionrateInput, $paymentDate, $paymentDateEnd)==true){

	  //---------------------------send a post to controller (MainController.js)---------------------------//
      $.post(
				'/calculate',
				{name: $nameInput, lastName: $lastnameInput, annualsalary: $annualsalaryInput, pensionrate: $pensionrateInput, paymentDate: $paymentDate, paymentDateEnd: $paymentDateEnd},
				function (data) {
					//response

					//get the result
					var name = data.name;
					var lastName = data.lastName;
					var paymentDate = data.paymentDate;
					var grossincome = data.grossincome;
					var incometax = data.incometax;
					var netincome = data.netincome;
					var pensioncontribuition = data.pensioncontribuition;
					var paymentDateEnd = data.paymentDateEnd;

					$('#nameResult').html(name+" "+lastName);
					$('#paymentDateResult').html(paymentDate+' - '+ paymentDateEnd);
					$('#grossIncomeResult').html("$ "+grossincome);
					$('#incomeTaxResult').html("$ "+incometax);
					$('#netIncomeResult').html("$ "+netincome);
					$('#pensionContribuitionResult').html("$ "+pensioncontribuition);

					window.location = '#result';

				}
			).fail(function(res){
				alert("Error: " + res.getResponseHeader("error"));
		});
		//---------------------------send a post to controller (MainController.js) END---------------------------//
	}
	else{
		//if the user does not complete all forms
		alert('Complete the form !');
	}
	//----------------------form validate END----------------------//

});

function currencyToNumber(valor)
{
	//change currency to number
	try{
		return Number(valor.replace(/[^0-9\.-]+/g,""));	
	}
	catch(err){
		console.log("ERROR");
	}
    
}

//--------------------------Test harness------------------------//
function validateForm($nameInput, $lastnameInput, $annualsalaryInput, $pensionrateInput, $paymentDate, $paymentDateEnd){
	//check if the value of the forms is null
	var status = 0;

	if($nameInput=='' || $nameInput==undefined){
		status = 1;
	}
	else{
		if(status!=1){
			status = 0;
		}
	}

	if($lastnameInput=='' || $lastnameInput==undefined){
		status = 1;
	}
	else{
		if(status!=1){
			status = 0;
		}
	}

	if(($annualsalaryInput=='' || $annualsalaryInput==undefined) || $annualsalaryInput<0){
		status = 1;
	}
	else{
		if(status!=1){
			status = 0;
		}
	}

	if(($pensionrateInput=='' || $pensionrateInput==undefined) || $pensionrateInput<=0 || $pensionrateInput>100){
		status = 1;
		alert("Maximum 100 in the pension rate field");
	}
	else{
		if(status!=1){
			status = 0;
		}
		
	}

	if($paymentDate=='' || $paymentDate==undefined){
		status = 1;
	}
	else{
		if(status!=1){
			status = 0;
		}
	}

	if($paymentDateEnd=='' || $paymentDateEnd==undefined){
		status = 1;
	}
	else{
		if(status!=1){
			status = 0;
		}
	}

	//status 1 = error validate
	//status 0 = no error
	if(status == 1){
		return false;
	}
	else{
		return true;
	}

}
//--------------------------test harness END------------------------//
